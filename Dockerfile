# develop stage
FROM node:12 as develop-stage
WORKDIR /app
COPY package*.json ./
RUN npm install -g @quasar/cli
# build stage
FROM develop-stage as build-stage
RUN npm install
COPY . .
RUN quasar build
# production stage
FROM develop-stage as production-stage
EXPOSE 9191
COPY --from=build-stage /app/dist/spa .
CMD ["quasar", "serve", "-p 9191"]