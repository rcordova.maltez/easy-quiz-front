import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import auth from './auth'
import current from './current'

Vue.use(Vuex)

/* TODO: pasar a documentación
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

const Store = new Vuex.Store({
  modules: {
    auth, current
  },

  plugins: [createPersistedState()],

  /* TODO: pasar a documentación (es importante esto igual)
  enable strict mode (adds overhead!)
   for dev mode only */
  strict: process.env.DEV
})

/* TODO: pasar a documentación (es importante esto igual)
  if we want some HMR magic for it, we handle
  the hot update like below. Notice we guard this
  code with "process.env.DEV" -- so this doesn't
  get into our production build (and it shouldn't).
*/

if (process.env.DEV && module.hot) {
  module.hot.accept(['./auth'], () => {
    const newAuth = require('./auth').default
    const newCurrent = require('./current').default
    Store.hotUpdate({ modules: { auth: newAuth, current: newCurrent } })
  })
}

export default Store
