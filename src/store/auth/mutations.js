
export function loggingIn (state, val) {
  state.isLoggingIn = val
}

export function loggedIn (state, val) {
  state.isLoggedIn = val
}

export function setName (state, val) {
  state.fullName = val
}

export function setId (state, val) {
  state.id = val
}

export function setAuth (state, val) {
  state.authToken = val
}
