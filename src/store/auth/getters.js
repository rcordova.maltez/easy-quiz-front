
export function loggingIn (state) {
  return state.isLoggingIn
}

export function loggedIn (state) {
  return state.isLoggedIn
}

export function getName (state) {
  return state.fullName
}

export function getId (state) {
  return state.id
}

export function getAuth (state) {
  return state.authToken
}
