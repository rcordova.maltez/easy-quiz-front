import { Notify } from 'quasar'
import { post, axiosInstance } from 'boot/axios'
import { validate } from 'boot/jwt'

export function logIn ({ commit }, form) {
  commit('loggingIn', true)
  post('login', form, { 'Access-Control-Expose-Headers': 'Authorization' })
    .then((response) => {
      validate(response.headers.authorization)
        .then((decoded) => {
          commit('loggedIn', true)
          commit('setName', decoded.sub)
          commit('setId', decoded.jti)
          commit('setAuth', response.headers.authorization)
          Notify.create({ color: 'positive', textColor: 'white', icon: 'check_circle', message: 'Sesión inicada con éxito.' })
        })
        .catch(() => {
          Notify.create({ color: 'negative', textColor: 'white', icon: 'cancel', message: 'Error al iniciar sesión' })
        })
    })
    .catch(() => {
      Notify.create({ color: 'negative', textColor: 'white', icon: 'cancel', message: 'Error al iniciar sesión' })
    })
    .finally(() => {
      commit('loggingIn', false)
    })
}

export function logOut ({ commit, dispatch }) {
  commit('loggedIn', false)
  commit('setName', 'John Doe')
  commit('setId', '0')
  commit('setAuth', '')
  axiosInstance.defaults.headers.common.Authorization = null
  dispatch('current/clearList', { root: true })
  Notify.create({ color: 'warning', textColor: 'white', icon: 'error', message: 'Sesión cerrada con éxito.' })
}
