
export function getCurrentQuiz (state) {
  return state.currentQuiz
}

export function getQuestionList (state) {
  return state.questionList
}

export function getWorkState (state) {
  return state.working
}

export function getCurrentThemes (state) {
  return state.currentThemes
}

export function getCurrentSkills (state) {
  return state.currentSkills
}

export function getCurrentArea (state) {
  return state.currentArea
}
