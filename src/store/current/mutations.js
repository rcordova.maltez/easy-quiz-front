
export function setCurrentQuiz (state, val) {
  state.currentQuiz = val
}

export function setCurrentSkills (state, val) {
  state.currentSkills = val
}

export function setCurrentArea (state, val) {
  state.currentArea = val
}

export function removeSkill (state, val) {
  const index = state.currentSkills.indexOf(val)
  if (index > -1) {
    if (val.number > 0) {
      state.currentSkills[index].number -= 1
    } else {
      state.currentSkills.splice(index, 1)
    }
  }
}

export function setCurrentThemes (state, val) {
  state.currentThemes = val
}

export function removeTheme (state, val) {
  const index = state.currentThemes.indexOf(val)
  if (index > -1) {
    if (val.number > 0) {
      state.currentThemes[index].number -= 1
    } else {
      state.currentThemes.splice(index, 1)
    }
  }
}

export function setQuestionList (state, val) {
  state.questionList = val
  for (const qSelected of state.questionSelected) {
    const found = state.questionList.some(qListed => qListed.id === qSelected.id)
    if (!found) {
      state.questionList.push(qSelected)
    }
  }
  state.questionSelected = []
}

export function emptyQuestionList (state) {
  state.questionList = []
}

export function addQuestionToSelected (state, val) {
  state.questionSelected.push(val)
}

export function removeQuestionFromList (state, val) {
  const index = state.questionList.indexOf(val)
  if (index > -1) {
    state.questionList.splice(index, 1)
  }
}

export function changeWorkState (state, val) {
  state.working = val
}

export function clearCurrentWork (state) {
  state.questionList = []
  state.questionSelected = []
  state.currentThemes = []
  state.currentArea = []
  state.currentSkills = []
  state.currentQuiz = {}
}
