import { Notify } from 'quasar'
import { post } from 'boot/axios'

export function addNewQuestion ({ commit }, form) {
  return new Promise((resolve, reject) => {
    commit('changeWorkState', true)
    post('api/question/new', form)
      .then((response) => {
        commit('addQuestionToSelected', response.data)
        Notify.create({
          color: 'positive',
          textColor: 'white',
          icon: 'cloud',
          message: 'Pregunta guardada con éxito.'
        })
        resolve(true)
      })
      .catch((error) => {
        Notify.create({
          color: 'negative',
          textColor: 'white',
          icon: 'cloud_off',
          message: 'Error al guardar la pregunta. Por favor, vuelva a intentar más tarde.'
        })
        reject(error)
      })
      .finally(() => {
        commit('changeWorkState', false)
      })
  })
}

export function setLocalQuiz ({ commit }, quiz) {
  return new Promise((resolve, reject) => {
    commit('setCurrentQuiz', quiz)
    resolve(true)
  }).catch(error => Promise.reject(error))
}

export function setLocalTags ({ commit }, [area, themes, skills]) {
  return new Promise((resolve, reject) => {
    commit('setCurrentThemes', themes)
    commit('setCurrentSkills', skills)
    commit('setCurrentArea', area)
    resolve(true)
  }).catch(error => Promise.reject(error))
}

export function setCurrentQuiz ({ commit }, [form, area, themes, skills]) {
  return new Promise((resolve, reject) => {
    commit('changeWorkState', true)
    post('api/quiz/new', form)
      .then((response) => {
        commit('setCurrentQuiz', response.data)
        commit('setCurrentThemes', themes)
        commit('setCurrentSkills', skills)
        commit('setCurrentArea', area)
        Notify.create({
          color: 'positive',
          textColor: 'white',
          icon: 'cloud',
          message: 'Evaluación creada con éxito.'
        })
        resolve(true)
      })
      .catch((error) => {
        Notify.create({
          color: 'negative',
          textColor: 'white',
          icon: 'cloud_off',
          message: 'Error al guardar la evaluación. Por favor, vuelva a intentar más tarde.'
        })
        reject(error)
      })
      .finally(() => {
        commit('changeWorkState', false)
      })
  })
}

export function setQuestionList ({ commit }, form) {
  return new Promise((resolve, reject) => {
    commit('changeWorkState', true)
    post('api/question/suggestions', form)
      .then((response) => {
        commit('setQuestionList', response.data)
        resolve(true)
      })
      .catch((error) => {
        Notify.create({
          color: 'negative',
          textColor: 'white',
          icon: 'cloud_off',
          message: 'Hubo un problema al cargar las sugerencias. Por favor, vuelvalo a intentar más tarde.'
        })
        reject(error)
      })
      .finally(() => {
        commit('changeWorkState', false)
      })
  })
}

export function sendCurrentQuiz ({ commit, state }, form) {
  return new Promise((resolve, reject) => {
    commit('changeWorkState', true)
    post('api/quiz/update', {
      quiz: state.currentQuiz,
      questionDto: form
    })
      .then((response) => {
        commit('setCurrentQuiz', response.data)
        Notify.create({
          color: 'positive',
          textColor: 'white',
          icon: 'cloud',
          message: 'Evaluación actualizada con éxito.'
        })
        resolve(true)
      })
      .catch((error) => {
        Notify.create({
          color: 'negative',
          textColor: 'white',
          icon: 'cloud_off',
          message: 'Error al guardar la evaluación. Por favor, vuelva a intentar más tarde.'
        })
        reject(error)
      })
      .finally(() => {
        commit('changeWorkState', false)
      })
  })
}

export function endCurrentQuiz ({ commit, state }, send) {
  return new Promise((resolve, reject) => {
    if (send) {
      post('api/quiz/save', state.currentQuiz)
        .then(() => {
          commit('clearCurrentWork')
          Notify.create({
            color: 'positive',
            textColor: 'white',
            icon: 'cloud',
            message: 'Evaluación se encontraba lista y fue guardada con éxito.'
          })
          resolve(true)
        })
        .catch((error) => {
          Notify.create({
            color: 'negative',
            textColor: 'white',
            icon: 'cloud_off',
            message: 'Error al guardar la evaluación. Por favor, vuelva a intentar más tarde.'
          })
          reject(error)
        })
    } else {
      commit('clearCurrentWork')
      Notify.create({
        color: 'positive',
        textColor: 'white',
        icon: 'cloud',
        message: 'Evaluación fue guardada con éxito.'
      })
      resolve(true)
    }
  })
}

export function addNewSugestion ({ commit, state }, question) {
  if (state.questionSelected.includes(question)) {
    Notify.create({
      color: 'info',
      textColor: 'white',
      icon: 'report',
      message: 'Pregunta ya se encuentra como sugerencia para esta sesión.'
    })
  } else {
    commit('addQuestionToSelected', question)
    Notify.create({
      color: 'positive',
      textColor: 'white',
      icon: 'done',
      message: 'Pregunta guardada como sugerencia para esta sesión.'
    })
  }
}

export function clearList ({ commit, state }) {
  if (Object.keys(state.currentQuiz).length === 0 && state.currentQuiz.constructor === Object) {
    commit('emptyQuestionList')
  }
}

export function removeQuestion ({ commit }, form) {
  commit('removeQuestionFromList', form)
}

export function rejectQuestion ({ commit }, form) {
  post('api/question/reject', {
    question: form.question,
    labels: form.labels,
    userId: form.userId
  })
  commit('removeQuestionFromList', form.question)
}
