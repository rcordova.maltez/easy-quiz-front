
const routes = [
  {
    path: '/',
    component: () => import('../layouts/MainLayout.vue'),
    redirect: '/home',
    meta: { requiresAuth: false },
    children: [
      {
        path: 'home',
        name: 'home',
        component: () => import('../pages/Index.vue'),
        props: { title: 'Inicio', caption: 'Página de inicio', icon: 'home' }
      },
      {
        path: 'registration',
        component: () => import('../pages/Registration.vue'),
        props: { title: 'Registro', caption: 'Página de Registro', icon: 'create' }
      }
    ]
  },
  {
    path: '/app',
    component: () => import('../layouts/MainLayout.vue'),
    redirect: '/home',
    meta: { requiresAuth: true },
    children: [
      {
        path: '/home',
        name: 'app',
        component: () => import('../pages/Index.vue'),
        props: { title: 'Inicio', caption: 'Página de inicio', icon: 'home' }
      },
      {
        path: '/quiz/creation',
        name: 'quizCreation',
        component: () => import('../pages/quiz/QuizCreation.vue'),
        props: { title: 'Crear Quiz', caption: 'Crear quizzes nuevos', icon: 'add_circle' }
      },
      {
        path: '/quiz/revision',
        name: 'quizRevision',
        component: () => import('../pages/quiz/QuizRevision.vue'),
        props: { title: 'Revisar Quiz', caption: 'Revisar quizzes creados', icon: 'track_changes' }
      },
      {
        path: '/question/creation',
        name: 'questionCreation',
        component: () => import('../pages/question/QuestionCreation.vue'),
        props: { title: 'Crear Question', caption: 'Crear preguntas nuevas', icon: 'add_circle_outline' }
      },
      {
        path: '/question/revision',
        name: 'questionRevision',
        component: () => import('../pages/question/QuestionRevision.vue'),
        props: { title: 'Revisar Question', caption: 'Revisar preguntas creadas', icon: 'find_replace' }
      }
    ]
  },
  {
    path: '/extras',
    component: () => import('../layouts/MainLayout.vue'),
    redirect: '/home',
    meta: { requiresAuth: true },
    children: [
      {
        path: '/quiz/revision/update',
        name: 'quizUpdate',
        component: () => import('../pages/quiz/QuizUpdate.vue'),
        props: { title: 'Quiz', caption: 'Modificar quizzes creados', icon: 'update' }
      },
      {
        path: '/quiz/creation/add',
        name: 'quizAdd',
        component: () => import('../pages/quiz/QuizCreationAdd.vue'),
        beforeEnter: (to, from, next) => {
          if (from.path !== '/quiz/creation' || !to.query.creating) next({ path: '/quiz/creation', query: { error: true } })
          else next()
        },
        props: { title: 'Quiz', caption: 'Añadir preguntas a quizz', icon: 'queue' }
      },
      {
        path: '/home/settings',
        name: 'configPerfil',
        component: () => import('../pages/Config.vue'),
        props: { title: 'Config', caption: 'Configuración de la cuenta de usuario', icon: 'settings' }
      }
    ]
  }
]

// Always leave this as last one
routes.push({
  path: '*',
  component: () => import('../pages/Error404.vue')
})

export default routes
