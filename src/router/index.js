import Vue from 'vue'
import VueRouter from 'vue-router'
import { axiosInstance } from 'boot/axios'
import store from '../store'
import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

const Router = new VueRouter({
  scrollBehavior: () => ({ x: 0, y: 0 }),
  routes,

  // Leave these as they are and change in quasar.conf.js instead!
  // quasar.conf.js -> build -> vueRouterMode
  // quasar.conf.js -> build -> publicPath
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE
})

Router.beforeEach((to, from, next) => {
  to.matched.some((route) => {
    if (route.meta.requiresAuth) {
      if (!store.getters['auth/loggedIn']) {
        axiosInstance.defaults.headers.common.Authorization = null
        next({ path: '/' })
      } else {
        axiosInstance.defaults.headers.common.Authorization = store.getters['auth/getAuth']
      }
    }
    next()
  })
})

export default Router
