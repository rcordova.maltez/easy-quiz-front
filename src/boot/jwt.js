import { verify } from 'jsonwebtoken'
import { secretKey } from './secretKey.js'

const validate = async function (token) {
  try {
    return verify(token.split(' ')[2], Buffer.from(secretKey, 'base64'), { algorithms: ['HS512'] })
  } catch (err) {
    return err
  }
}

export { validate }
