import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: 'http://localhost:9090/',
  headers: {
    Accept: '*/*',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Expose-Headers': '*'
  }
})

const get = async function (url, params, headers) {
  return axiosInstance.get(url, { params }, { headers })
}

const post = async function (url, data, headers, responseType) {
  return axiosInstance.post(url, data, { headers, responseType: responseType })
}

export default async ({ Vue }) => {
  Vue.prototype.$connection = { get, post, axiosInstance }
}

export { get, post, axiosInstance }
